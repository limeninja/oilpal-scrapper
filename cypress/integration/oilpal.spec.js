/// <reference types="Cypress" />

let oilpal_days_to_empty = '';
let oilpal_last_contact = '';
let oilpal_amount_to_fill = '';
let oilpal_fuel_type = '';
let oilpal_remaining_fuel = '';

function NumberWithLeadingZeroes(n) {
  if (n < 10) {
    return '000' + n.toString();
  } else if (n < 100) {
    return '00' + n.toString();
  } else if (n < 1000) {
    return '0' + n.toString();
  } else {
    return n;
  }
}

context('Oilpal', () => {
  beforeEach(() => {});

  it('Login and get info', () => {
    cy.visit('http://app.oilpal.com');
    // https://on.cypress.io/type
    cy.get('#UserName')
      .type('rob_stokes@me.com')
      .should('have.value', 'rob_stokes@me.com');

    cy.get('#Password').type('forsaken');

    cy.get('button')
      .contains('Login')
      .click();

    cy.get('h3').each((h, index) => {
      const value = h[0].innerHTML.replace(/ /g, '').replace(/(?:\r\n|\r|\n)/g, '');
      switch (index) {
        case 0: {
          console.log('Days to empty: ', value);
          oilpal_days_to_empty = value;
          break;
        }
        case 1: {
          console.log('Last contact: ', value);
          oilpal_last_contact = value;
          break;
        }
        case 2: {
          console.log('Amount to fill: ', value);
          oilpal_amount_to_fill = value.replace('L', '');
          break;
        }
        case 3: {
          console.log('Fuel Type: ', value);
          oilpal_fuel_type = value;
          break;
        }

        case 4: {
          console.log('Remaining Fuel: ', value);
          oilpal_remaining_fuel = value.replace('Litres', '');
          break;
        }
        default:
          break;
      }
    });
  });

  it('send results', () => {
    cy.request(
      `https://nodered.pidgeonsnest.uk/endpoint/homeassistant/sensor.oilpal_days_to_empty?value=${oilpal_days_to_empty}`
    );
    cy.request(
      `https://nodered.pidgeonsnest.uk/endpoint/homeassistant/sensor.oilpal_last_contact?value=${oilpal_last_contact}`
    );
    cy.request(
      `https://nodered.pidgeonsnest.uk/endpoint/homeassistant/sensor.oilpal_amount_to_fill?value=${oilpal_amount_to_fill}`
    );
    cy.request(
      `https://nodered.pidgeonsnest.uk/endpoint/homeassistant/sensor.oilpal_fuel_type?value=${oilpal_fuel_type}`
    );
    cy.request(
      `https://nodered.pidgeonsnest.uk/endpoint/homeassistant/sensor.oilpal_remaining_fuel?value=${oilpal_remaining_fuel}`
    );
  });
});
